<?php
// ajax long polling
header('Content-Type: application/json');
include("pass.php");  //require

try {
    $dbh = new PDO($host, $user, $passwd);
} catch (PDOException $e) {
    echo 'Connection failed: ' . $e->getMessage();
}

if( isset($_POST['f']) && $_POST['f']==0 ){
    
    
	
        $t1 = "";
        $t2 = "X, O";
        
        /*
	$sth = $dbh->prepare('insert into ox(thisPlayer, players, fields) values(:thisPlayer, :players, :field);');
        $sth->bindValue(':thisPlayer', $t1, PDO::PARAM_STR);
	$sth->bindValue(':players', $t1, PDO::PARAM_STR);
	$sth->bindValue(':field', $t1, PDO::PARAM_STR);
	$sth->execute();
	*/
	
    
	$sth = $dbh->prepare('UPDATE ox SET thisPlayer=:thisPlayer, players=:players, fields=:field, accPlayer=:accPlayer, win=:win WHERE id=32');
	$sth->bindValue(':thisPlayer', $t1, PDO::PARAM_STR);
	$sth->bindValue(':players', $t2, PDO::PARAM_STR);
	$sth->bindValue(':field', $t1, PDO::PARAM_STR);
    $sth->bindValue(':accPlayer', $t1, PDO::PARAM_STR);
    $sth->bindValue(':win', $t1, PDO::PARAM_STR);
	$sth->execute();
     
    $sthh = $dbh->prepare('select * from ox');
	$sthh->execute();
	$result = $sthh->fetchAll();
	echo json_encode($result); 
    
            
    

    }else if( isset($_POST['f']) && $_POST['f']==1 ){
	//echo json_encode( array( 'imie' => ' '.$_POST['imie'] ) );
	//file_put_contents("req.txt", $q);
        // tabela - ox ; ID, players, thisPlayer, fields

    $q = json_decode(rawurldecode($_POST['game'])); // "{}"
	
    
    //print_r($q);
    
    
    $str = "";
    
    $playX = 0;
    $playO = 0;
    
    for($i = 0; $i<count($q->{'fields'}); $i++){
    	
    	if($i == count($q->{'fields'})-1){
    		$str .= $q->{'fields'}[$i];
    	}else{
    		$str .= $q->{'fields'}[$i].", ";
    	}
    	
    	if($q->{'fields'}[$i]=="X"){
    		$playX = $playX + 1;
    	}elseif($q->{'fields'}[$i]=="O"){
    		$playO = $playO + 1;
    	}
    }
	    
    
        
    //SQL UPDATE  
	
	$sth = $dbh->prepare('UPDATE ox SET fields=:field WHERE id=32');
	$sth->bindValue(':field', $str, PDO::PARAM_STR);
	$sth->execute();
               
	
	if($playX  > $playO){
		$sth = $dbh->prepare('UPDATE ox SET accPlayer=:accPlayer WHERE id=32');
		$sth->bindValue(':accPlayer', 'O', PDO::PARAM_STR);
		$sth->execute();
	}elseif($playX  < $playO){
		$sth = $dbh->prepare('UPDATE ox SET accPlayer=:accPlayer WHERE id=32');
		$sth->bindValue(':accPlayer', 'X', PDO::PARAM_STR);
		$sth->execute();
	}elseif($playX  == $playO){
		$sth = $dbh->prepare('UPDATE ox SET accPlayer=:accPlayer WHERE id=32');
		
		$sql = $dbh->prepare('SELECT thisPlayer FROM ox');
		$sql->execute();
		$result = $sql->fetchAll();
		//print_r($result);
		
		
		$sth->bindValue(':accPlayer', $result[0]['thisPlayer'], PDO::PARAM_STR);
		$sth->execute();
	}
	
	
	
	$sthh = $dbh->prepare('select * from ox');
	$sthh->execute();
	$result = $sthh->fetchAll();
	//print_r($result);
	
	$win = $dbh->prepare('select fields from ox');
	$win->execute();
	$winRes = $win->fetchAll();
        
        $winPlayer = "";
        $winTab = explode(', ', $winRes[0]['fields']);
        
        $remis = 0;
        
        for($i = 0;$i < count($winTab);$i++){
        	if($winTab[$i] != "empty"){
        		$remis = $remis + 1;
        	}
        }
        

        
        if(
                ($winTab[0] == "X" && $winTab[1] == "X" && $winTab[2] == "X") ||
                ($winTab[3] == "X" && $winTab[4] == "X" && $winTab[5] == "X") ||
                ($winTab[6] == "X" && $winTab[7] == "X" && $winTab[8] == "X") ||
                
                ($winTab[0] == "X" && $winTab[3] == "X" && $winTab[6] == "X") ||
                ($winTab[1] == "X" && $winTab[4] == "X" && $winTab[7] == "X") ||
                ($winTab[2] == "X" && $winTab[5] == "X" && $winTab[8] == "X") ||
                
                ($winTab[0] == "X" && $winTab[4] == "X" && $winTab[8] == "X") ||
                ($winTab[2] == "X" && $winTab[4] == "X" && $winTab[6] == "X") 
                ){
            $winPlayer .= "X";
            
            $sth = $dbh->prepare('UPDATE ox SET win=:winPlayer WHERE id=32');
            $sth->bindValue(':winPlayer', $winPlayer, PDO::PARAM_STR);
            $sth->execute();
        }elseif(
                ($winTab[0] == "O" && $winTab[1] == "O" && $winTab[2] == "O") ||
                ($winTab[3] == "O" && $winTab[4] == "O" && $winTab[5] == "O") ||
                ($winTab[6] == "O" && $winTab[7] == "O" && $winTab[8] == "O") ||
                
                ($winTab[0] == "O" && $winTab[3] == "O" && $winTab[6] == "O") ||
                ($winTab[1] == "O" && $winTab[4] == "O" && $winTab[7] == "O") ||
                ($winTab[2] == "O" && $winTab[5] == "O" && $winTab[8] == "O") ||
                
                ($winTab[0] == "O" && $winTab[4] == "O" && $winTab[8] == "O") ||
                ($winTab[2] == "O" && $winTab[4] == "O" && $winTab[6] == "O") 
                ){
            $winPlayer .= "O";
            
            $sth = $dbh->prepare('UPDATE ox SET win=:winPlayer WHERE id=32');
            $sth->bindValue(':winPlayer', $winPlayer, PDO::PARAM_STR);
            $sth->execute();
        }
        
        $sqlRem = $dbh->prepare('SELECT win FROM ox');
        $sqlRem->execute();
        $resultRem = $sqlRem->fetchAll();
        
        
        if($remis == 9 && ($resultRem[0]['win'] != "O" || $resultRem[0]['win'] != "X")){
        
            $winPlayer .= "Remis";
            
            $sth = $dbh->prepare('UPDATE ox SET win=:winPlayer WHERE id=32');
            $sth->bindValue(':winPlayer', $winPlayer, PDO::PARAM_STR);
            $sth->execute();
        
        }
		
	echo json_encode($result); 
	
	

    
}else if( isset($_POST['f']) && $_POST['f']==2){
    
    $sthh = $dbh->prepare('select * from ox');
	$sthh->execute();
	$result = $sthh->fetchAll();
	echo json_encode($result); 
    
}else if( isset($_POST['f']) && $_POST['f']==3 ){
    
    $q = json_decode(rawurldecode($_POST['game'])); // "{}"
	
    $str = "";
    
    $playX = 0;
    $playO = 0;
    
    for($i = 0; $i<count($q->{'fields'}); $i++){
    	
    	if($i == count($q->{'fields'})-1){
    		$str .= $q->{'fields'}[$i];
    	}else{
    		$str .= $q->{'fields'}[$i].", ";
    	}
    	
    	if($q->{'fields'}[$i]=="X"){
    		$playX = $playX + 1;
    	}elseif($q->{'fields'}[$i]=="O"){
    		$playO = $playO + 1;
    	}
    }
	    
    
        $accPlay = "";
        if($q->{'players'}=="X"){
            $accPlay = "set, O";
        }elseif($q->{'players'}=="O"){
            $accPlay = "set, X";
        }
        
    //SQL UPDATE  
	
	//$sth = $dbh->prepare('insert into ox(thisPlayer, players, fields) values(:thisPlayer, :players, :field);');
	$sth = $dbh->prepare('UPDATE ox SET thisPlayer=:thisPlayer,players=:players, fields=:field WHERE id=32');
	$sth->bindValue(':thisPlayer', $q->{'thisPlayer'}, PDO::PARAM_STR);
    $sth->bindValue(':players', $accPlay, PDO::PARAM_STR);
	$sth->bindValue(':field', $str, PDO::PARAM_STR);
	$sth->execute();
               
	
	if($playX  > $playO){
		$sth = $dbh->prepare('UPDATE ox SET accPlayer=:accPlayer WHERE id=32');
		$sth->bindValue(':accPlayer', 'O', PDO::PARAM_STR);
		$sth->execute();
	}elseif($playX  < $playO){
		$sth = $dbh->prepare('UPDATE ox SET accPlayer=:accPlayer WHERE id=32');
		$sth->bindValue(':accPlayer', 'X', PDO::PARAM_STR);
		$sth->execute();
	}elseif($playX  == $playO){
		$sth = $dbh->prepare('UPDATE ox SET accPlayer=:accPlayer WHERE id=32');
		
		$sql = $dbh->prepare('SELECT thisPlayer FROM ox');
		$sql->execute();
		$result = $sql->fetchAll();
		//print_r($result);
		
		
		$sth->bindValue(':accPlayer', $result[0]['thisPlayer'], PDO::PARAM_STR);
		$sth->execute();
	}
	
	
	
	$sthh = $dbh->prepare('select * from ox');
	$sthh->execute();
	$result = $sthh->fetchAll();
	//print_r($result);
	

	
	echo json_encode($result); 
	
    
}

?>
